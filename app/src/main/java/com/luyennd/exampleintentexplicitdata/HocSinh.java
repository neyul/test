package com.luyennd.exampleintentexplicitdata;

import java.io.Serializable;

public class HocSinh implements Serializable {

    private String name, age;

    public HocSinh(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
