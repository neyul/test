package com.luyennd.exampleintentexplicitdata;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        initView();
    }

    private void initView() {
        tvResult = findViewById(R.id.tv_result);
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("test");
//        HocSinh hs = (HocSinh) intent.getSerializableExtra("test");
        String ten = bundle.getString("chuoi");
        tvResult.setText(ten);
    }
}
