package com.luyennd.exampleintentexplicitdata;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
        btSend = findViewById(R.id.bt_send);

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);

                String[] arrName = {"yêu", "anh", "đi", "em"};
                HocSinh hocSinh = new HocSinh("Đức Luyện", "28");
                Bundle bundle = new Bundle();
                bundle.putString("chuoi", "hello");
                bundle.putInt("so", 1234);
                bundle.putStringArray("mangchuoi", arrName);
                intent.putExtra("test", bundle);

                startActivity(intent);
            }
        });
    }
}
